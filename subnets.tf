
resource "aws_subnet" "main" {
    count = 3
    vpc_id = aws_vpc.main.id
    cidr_block = local.calculated_subnets[count.index]
    availability_zone = data.aws_availability_zones.available.names[count.index]
}
