
variable "cidr" {
    description = "The CIDR block of the VPC"
    type = string
    default = "10.1.0.0/16"
}

variable "subnet_size" {
    description = "The /size of the subnets to create"
    type = string
    default = "24"
}
