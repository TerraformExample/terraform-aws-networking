
output "vpc_id" {
    value = aws_vpc.main.id
}

output "subnet_az_a_id" {
    value = aws_subnet.main[0].id
}

output "subnet_az_b_id" {
    value = aws_subnet.main[1].id
}

output "subnet_az_c_id" {
    value = aws_subnet.main[2].id
}
